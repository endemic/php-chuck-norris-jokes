### Set up
    
```
git clone https://gitlab.com/endemic/php-chuck-norris-jokes
cd ./cv-test-jokes
```

Copy **.env-dist** to **.env**
Set email setting with string format as in *.env*

```
docker-compose up -d
docker-compose exec app bash
composer install
php bin/phpunit
```